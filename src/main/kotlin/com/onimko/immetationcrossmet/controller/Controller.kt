package com.onimko.immetationcrossmet.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v3/status/sensor-value-group")
class Controller(
    @Value("\${test.json}" ) private val testJson:String
) {

    @GetMapping("/Meteo")
    fun  meteo():ResponseEntity<String>{
        println("RESPONSE!!!")
        return  ResponseEntity.ok(testJson)
    }
}