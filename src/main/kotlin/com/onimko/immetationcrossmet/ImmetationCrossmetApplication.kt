package com.onimko.immetationcrossmet

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ImmetationCrossmetApplication

fun main(args: Array<String>) {
    runApplication<ImmetationCrossmetApplication>(*args)
}
